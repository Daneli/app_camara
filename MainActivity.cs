﻿using System;
using Android.Provider;
using Android.Runtime;
using Android.Views;
using AndroidX.AppCompat.Widget;
using AndroidX.AppCompat.App;
using Google.Android.Material.FloatingActionButton;
using Google.Android.Material.Snackbar;

using Android.Widget;
using Uri = Android.Net.Uri;
using Path = System.IO.Path;
using System.IO;
using Plugin.Geolocator;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Android.Content;
using Android.App;
using Android.OS;
using Android.Graphics;

namespace App_AzureStorage
{
    [Activity(Label = "Android_Azure", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        int camrequestcode = 100;
        Uri FileUri;
        ImageView Image;
        EditText txtName, txtAddress, txtAge, txtEmail, txtRevenue;
        FileStream streamFile;
        double lat, lon;
        Intent intent;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);
            SetContentView(Resource.Layout.activity_main);

            var btnStorage = FindViewById<Button>(Resource.Id.btnStorage);
            txtName = FindViewById<EditText>(Resource.Id.txtName);
            txtAddress = FindViewById<EditText>(Resource.Id.txtAddress);
            txtAge = FindViewById<EditText>(Resource.Id.txtAge);
            txtEmail = FindViewById<EditText>(Resource.Id.txtEmail);
            txtRevenue = FindViewById<EditText>(Resource.Id.txtRevenue);
            Image = FindViewById<ImageView>(Resource.Id.image);



            btnStorage.Click += async delegate
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;
                    var position = await locator.GetPositionAsync
                        (TimeSpan.FromSeconds(10), null, true);
                    lat = position.Latitude;
                    lon = position.Longitude;

                    var StorageAccount = CloudStorageAccount.Parse
                    ("DefaultEndpointsProtocol=https;AccountName=danelireyes;AccountKey=f/erjBfcReYACyddwlEWV+BxHrP2PgUKalKlKs2p1bJRzJREM0LgjtLuCVitNiIrU66vU08IwrHDJsBNwSm+7A==;EndpointSuffix=core.windows.net");
                    var TableNoQL = StorageAccount.CreateCloudTableClient();
                    var Table = TableNoQL.GetTableReference("administrativa");
                    await Table.CreateIfNotExistsAsync();

                    var employee = new Employees("administrativa Staff", txtName.Text);
                    employee.Mail = txtEmail.Text;
                    employee.Revenue = double.Parse(txtRevenue.Text);
                    employee.Age = int.Parse(txtAge.Text);
                    employee.Address = txtAddress.Text;
                    employee.Latitude = lat;
                    employee.Longitude = lon;
                    employee.Image = txtName.Text + ".jpg";
                    var Store = TableOperation.Insert(employee);
                    await Table.ExecuteAsync(Store);
                    Toast.MakeText(this.ApplicationContext, "Data has been stored",
                        ToastLength.Long).Show();

                    var BlockClient = StorageAccount.CreateCloudBlobClient();
                    var Content = BlockClient.GetContainerReference("imagenespractica");
                    var resourceBlob = Content.GetBlockBlobReference(txtName.Text + ".jpg");
                    await resourceBlob.UploadFromFileAsync(FileUri.ToString());
                    Toast.MakeText(this.ApplicationContext, "Image stored in Azure Storage Container",
                        ToastLength.Long).Show();
                }
                catch (System.Exception ex)
                {
                    Toast.MakeText(this.ApplicationContext, ex.Message,
                        ToastLength.Long).Show();
                }
            };
            Image.Click += delegate
            {
                try
                {
                    intent = new Intent(MediaStore.ActionImageCapture);
                    intent.PutExtra(MediaStore.ExtraOutput, FileUri);
                    StartActivityForResult(intent, camrequestcode, bundle);
                }
                catch (System.Exception ex)
                {
                    Toast.MakeText(this.ApplicationContext, ex.Message,
                        ToastLength.Long).Show();
                }
            };
            
        }
        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if (requestCode == camrequestcode)
            {
                try
                {
                    if (txtName.Text != null)
                    {
                        FileUri = Android.Net.Uri.Parse(Path.Combine(System.Environment.GetFolderPath
                            (System.Environment.SpecialFolder.Personal), txtName.Text + ".jpg"));
                        if (File.Exists(FileUri.ToString()))
                        {
                            Toast.MakeText(this.ApplicationContext, "Image name already exists",
                            ToastLength.Long).Show();
                        }
                        else
                        {
                            streamFile = new FileStream(Path.Combine(System.Environment.GetFolderPath
                                (System.Environment.SpecialFolder.Personal), txtName.Text + ".jpg"),
                                FileMode.Create);
                            var bitmapImage = (Android.Graphics.Bitmap)data.Extras.Get("data");
                            bitmapImage.Compress(Bitmap.CompressFormat.Jpeg, 100, streamFile);
                            streamFile.Close();
                            Image.SetImageBitmap(bitmapImage);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Toast.MakeText(this.ApplicationContext, ex.Message,
                        ToastLength.Long).Show();
                }
            }
        }
        public class Employees : TableEntity
        {
            public Employees(string Category, string Name) 
            {
                PartitionKey = Category;
                RowKey = Name;
            }
            public string Mail { get; set; }
            public int Age { get; set; }
            public string Address { get; set; }
            public double Revenue { get; set; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }
            public string Image { get; set; }
        }
    }
}
